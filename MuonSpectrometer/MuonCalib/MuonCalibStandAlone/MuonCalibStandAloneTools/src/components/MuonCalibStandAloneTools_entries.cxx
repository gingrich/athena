#include "MuonCalibStandAloneTools/NtupleMTT0Tool.h"
#include "MuonCalibStandAloneTools/NtupleCalibADCTool.h"
#include "MuonCalibStandAloneTools/NtupleClassicT0Tool.h"
#include "MuonCalibStandAloneTools/NtupleIntegrationTool.h"
#include "MuonCalibStandAloneTools/NtupleAnalyticAutocalibrationTool.h"
#include "MuonCalibStandAloneTools/NtupleSimpleresolutionTool.h"
#include "MuonCalibStandAloneTools/NtupleChisqResolutionTool.h"
#include "MuonCalibStandAloneTools/NtupleTubeEfficiencyTool.h"
#include "MuonCalibStandAloneTools/NtupleRunScanTool.h"
#include "MuonCalibStandAloneTools/NtupleControlHistogramsTool.h"
#include "MuonCalibStandAloneTools/NtupleResidualTool.h"
#include "MuonCalibStandAloneTools/NtupleDisplayTool.h"
#include "MuonCalibStandAloneTools/NtupleWireCentricityTool.h"
#include "MuonCalibStandAloneTools/NtupleCurvedAutocalibrationTool.h"
#include "MuonCalibStandAloneTools/NtupleDBCheckTool.h"
#include "MuonCalibStandAloneTools/NtupleResidualVsTimeTool.h"

using namespace MuonCalib;

DECLARE_COMPONENT( NtupleMTT0Tool ) 
DECLARE_COMPONENT( NtupleCalibADCTool ) 
DECLARE_COMPONENT( NtupleClassicT0Tool ) 
DECLARE_COMPONENT( NtupleIntegrationTool ) 
DECLARE_COMPONENT( NtupleAnalyticAutocalibrationTool )
DECLARE_COMPONENT( NtupleChisqResolutionTool )
DECLARE_COMPONENT( NtupleSimpleResolutionTool )
DECLARE_COMPONENT( NtupleTubeEfficiencyTool )
DECLARE_COMPONENT( NtupleRunScanTool )
DECLARE_COMPONENT( NtupleControlHistogramsTool )
DECLARE_COMPONENT( NtupleResidualTool )
DECLARE_COMPONENT( NtupleDisplayTool )
DECLARE_COMPONENT( NtupleWireCentricityTool )
DECLARE_COMPONENT( NtupleCurvedAutocalibrationTool )
DECLARE_COMPONENT( NtupleDbCheckTool )
DECLARE_COMPONENT( NtupleResidualVsTimeTool )

